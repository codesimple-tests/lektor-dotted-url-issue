
def make_relative_url(base, target):
    """Returns a relative URL from base to target."""
    depth = base.lstrip('/').count('/')
    prefix = './' if depth == 0 else '../' * depth

    ends_in_slash = target[-1:] == '/'
    target = posixpath.normpath(posixpath.join(base, target))
    if ends_in_slash and target[-1:] != '/':
        target += '/'
    
    return prefix.rstrip('/') + target
